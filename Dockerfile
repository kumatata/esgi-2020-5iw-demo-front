FROM node:latest as deps
WORKDIR /app
COPY package.json .
COPY yarn.lock .
RUN yarn install

FROM nginx
COPY . /usr/share/ngnix/html/
COPY --from=deps /app/node_modules /usr/share/nginx/html
